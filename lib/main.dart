import 'dart:io';
import 'dart:ui' as ui;

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Face Detection',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: FaceDetection(),
    );
  }
}

class FaceDetection extends StatefulWidget {
  FaceDetection({Key key}) : super(key: key);

  @override
  _FaceDetectionState createState() => _FaceDetectionState();
}

class _FaceDetectionState extends State<FaceDetection> {
  var _isLoading = false;
  List<Face> _faces;
  File _imageFile;
  ui.Image _image;

  ImagePicker imagePicker = ImagePicker();

  _getImageAndFaceDetection() async {
    final pickedFile = await imagePicker.getImage(source: ImageSource.camera, preferredCameraDevice: CameraDevice.front);
    if (pickedFile == null) {
      return;
    }
    final tempImageFile = File(pickedFile.path);
    setState(() {
      _isLoading = true;
    });
    final image = FirebaseVisionImage.fromFile(tempImageFile);
    final faceDetector = FirebaseVision.instance.faceDetector();
    List<Face> tempFaces = await faceDetector.processImage(image);
    if (mounted) {
      if (tempFaces.length != 0) {
        // pesan konfirmasi ambil ulang gambar wajah
        _getImageAndFaceDetection();
        return;
      }
      setState(() {
        _imageFile = tempImageFile;
        _faces = tempFaces;
        _loadImage(tempImageFile);
      });
    }
  }

  _loadImage(File file) async {
    final data = await file.readAsBytes();
    await decodeImageFromList(data).then((value) => setState(() {
          _image = value;
          _isLoading = false;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : (_imageFile == null)
              ? Center(
                  child: Text("No Image Selected"),
                )
              : Center(
                  child: FittedBox(
                    child: SizedBox(
                      width: _image.width.toDouble(),
                      height: _image.height.toDouble(),
                      child: CustomPaint(
                        painter: FaceAreaPainter(_image, _faces),
                      ),
                    ),
                  ),
                ),
      floatingActionButton: FloatingActionButton(
        onPressed: _getImageAndFaceDetection,
        tooltip: "Pick Image",
        child: Icon(Icons.image),
      ),
    );
  }
}

class FaceAreaPainter extends CustomPainter {
  final ui.Image image;
  final List<Face> faces;
  final List<Rect> rects = [];

  FaceAreaPainter(this.image, this.faces) {
    for (var face in faces) {
      rects.add(face.boundingBox);
    }
  }

  @override
  void paint(ui.Canvas canvas, ui.Size size) {
    final Paint paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 15.0
      ..color = Colors.red[300];

    canvas.drawImage(image, Offset.zero, Paint());
    for (var rect in rects) {
      canvas.drawRect(rect, paint);
    }
  }

  @override
  bool shouldRepaint(FaceAreaPainter oldDelegate) {
    return image != oldDelegate.image || faces != oldDelegate.faces;
  }
}
